import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isChecked = true;
  data = 2;

  colors = [
    {id:1, name: 'Blue'},
    {id:2, name: 'Green'},
    {id:3, name: 'Red'}
  ];


  minDate = new Date(2018,1,1);
  maxDate = new Date(2018, 12, 31);

  isChanged($event){
    console.log($event);
    this.isChecked = !this.isChecked;
  }
}
